<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use App\Models\Country;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('countries')->insert([
            [
                'name' => 'United States of America',
            ],[
                'name' => 'South Africa',
            ],[
                'name' => 'Suede',
            ],[
                'name' => 'Japan',
            ],[
                'name' => 'Swiss',
            ],
        ]);
    }
}
