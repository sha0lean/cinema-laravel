<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CinemasTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('cinemas')->insert([
      [
        'name' => 'Cinema 1',
        'adress' => 'adress',
        'city' => 'ville',
        'country_id' => 1,
      ],      [
        'name' => 'Cinema 2',
        'adress' => 'adress',
        'city' => 'ville',
        'country_id' => 2,
      ],      [
        'name' => 'Cinema 3',
        'adress' => 'adress',
        'city' => 'ville',
        'country_id' => 3,
      ],      [
        'name' => 'Cinema 4',
        'adress' => 'adress',
        'city' => 'ville',
        'country_id' => 4,
      ]
    ]);
  }
}
