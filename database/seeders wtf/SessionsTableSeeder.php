<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use App\Models\Session;

class SessionsTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('sessions')->insert([
      [
        'begin' => '2021-03-15 15:30:00',
        'room_id' => 1,
        'movie_id' => 3
      ], [
        'begin' => '2021-03-15 13:30:00',
        'room_id' => 5,
        'movie_id' => 4
      ], [
        'begin' => '2021-03-15 10:30:00',
        'room_id' => 1,
        'movie_id' => 1
      ], [
        'begin' => '2021-03-15 19:30:00',
        'room_id' => 3,
        'movie_id' => 2
      ], [
        'begin' => '2021-03-15 14:30:00',
        'room_id' => 2,
        'movie_id' => 2
      ],[
        'begin' => '2021-03-15 9:30:00',
        'room_id' => 6,
        'movie_id' => 4
      ],
    ]);
  }
}
