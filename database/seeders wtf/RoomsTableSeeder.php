<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use App\Models\Room;

class RoomsTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('rooms')->insert([
      [
        'places' => 100,
        'cinema_id' => 1
      ], [
        'places' => 200,
        'cinema_id' => 2
      ], [
        'places' => 300,
        'cinema_id' => 3
      ], [
        'places' => 400,
        'cinema_id' => 4
      ], [
        'places' => 500,
        'cinema_id' => 5
      ], [
        'places' => 600,
        'cinema_id' => 6
      ],
    ]);
  }
}
