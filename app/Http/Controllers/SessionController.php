<?php

namespace App\Http\Controllers;

use App\Http\Requests\SessionRequest;
use App\Models\Cinema;
use App\Models\Movie;
use App\Models\Room;
use App\Models\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SessionController extends Controller
{
  /**
   * Class constructor
   */
  public function __construct()
  {
    $this->middleware('ajax')->only('destroy');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return view('sessions.index', ['sessions' => Session::paginate(10)]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create(Room $room, Movie $movie)
  {
    return view('sessions.create', [
      'rooms' => Room::all(),
      'movies' => Movie::all(),
      'room_selected' => $room,
      'movie_selected' => $movie
    ]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(SessionRequest $request)
  {
    Session::create($request->all());

    return redirect()->route('session.index')
      ->with('ok', __('Session has been saved !'));
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit(Session $session)
  {
    return view('sessions.edit', [
      'session' => $session,
      'rooms' => Room::all(),
      'movies' => Movie::all()
    ]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(SessionRequest $request, Session $session)
  {
    $session->update($request->all());

    return redirect()->route('session.index')
      ->with('ok', __('Session has been updated !'));
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Session $session)
  {
    $session->delete();

    return response()->json();
  }
}
