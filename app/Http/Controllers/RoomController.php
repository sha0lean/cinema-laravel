<?php

namespace App\Http\Controllers;

use App\Http\Requests\RoomRequest;
use App\Models\Cinema;
use App\Models\Room;
use Illuminate\Http\Request;
use Whoops\Run;

class RoomController extends Controller
{
  /**
   * Class constructor
   */
  public function __construct()
  {
    $this->middleware('ajax')->only('destroy');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return view('rooms.index', ['rooms' => Room::paginate(10)]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('rooms.create', ['cinemas' => Cinema::all()]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(RoomRequest $request)
  {
    Room::create($request->all());

    return redirect()->route('room.index')
      ->with('ok', __('Room has been saved !'));
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit(Room $room)
  {
    return view('rooms.edit', [
      'room' => $room,
    ]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(RoomRequest $request, Room $room)
  {
    $room->update($request->all());

    return redirect()->route('room.index')
      ->with('ok', __('Room has been updated !'));
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Room $room)
  {
    $room->delete();

    return response()->json();
  }
}
