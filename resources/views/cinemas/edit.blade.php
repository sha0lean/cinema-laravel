<x-app>
    <x-slot name="page_title">Edit {{ $cinema->name }}</x-slot>

    <form method="POST" action="{{ route('cinema.update', $cinema->id) }}">
        {{ csrf_field() }}
        {{ method_field('PUT') }}

        <p>
            <label for="name">Name</label>
            <input type="text" name="name" id="name" value="{{ $cinema->name }}" required class="block appearance-none w-full py-1 px-2 mb-1 text-base leading-normal bg-white text-grey-darker border border-grey rounded" />
            <x-error name="name"></x-error>
        </p>
        <p>
            <label for="adress">Address</label>
            <input type="text" name="adress" id="adress" value="{{ $cinema->adress }}" required class="block appearance-none w-full py-1 px-2 mb-1 text-base leading-normal bg-white text-grey-darker border border-grey rounded" />
            <x-error name="adress"></x-error>
        </p>
        <p>
            <label for="city">City</label>
            <input type="text" name="city" id="city" value="{{ $cinema->city }}" class="block appearance-none w-full py-1 px-2 mb-1 text-base leading-normal bg-white text-grey-darker border border-grey rounded" />
            <x-error name="city"></x-error>
        </p>

        <button type="submit" class="focus:outline-none text-white text-sm py-2.5 px-5 rounded-md bg-blue-500 hover:bg-blue-600 hover:shadow-lg">
            Update
        </button>
    </form>
</x-app>
