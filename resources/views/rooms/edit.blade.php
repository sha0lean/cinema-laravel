<x-app>
  <x-slot name="page_title">Edit {{ $room->cinema->name . ' ' . $room->id }}</x-slot>

  <form method="POST" action="{{ route('room.update', $room->id) }}">
    {{ csrf_field() }}
    {{ method_field('PUT') }}

    <p>
      <label for="places">Nb Places</label>
      <input type="text" name="places" id="places" value="{{ $room->places }}" required
        class="block appearance-none w-full py-1 px-2 mb-1 text-base leading-normal bg-white text-grey-darker border border-grey rounded" />
      <x-error name="places"></x-error>
    </p>
    <p>
      <label for="cinema_id">Cinema</label>
      <select name="cinema_id" id="cinema_id" required
        class="block appearance-none w-full py-1 px-2 mb-1 text-base leading-normal bg-white text-grey-darker border border-grey rounded">
        @foreach ($cinemas as $cinema)
          <option value="{{ $cinema->id }}">
            {{ $cinema->name }}
          </option>
        @endforeach
      </select>
      <x-error name="cinema_id"></x-error>
    </p>


    <button type="submit"
      class="focus:outline-none text-white text-sm py-2.5 px-5 rounded-md bg-blue-500 hover:bg-blue-600 hover:shadow-lg">
      Update
    </button>
  </form>
</x-app>
