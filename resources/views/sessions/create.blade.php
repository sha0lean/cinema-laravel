<x-app>
  <x-slot name="page_title">Create a session</x-slot>

  <form method="POST" action="{{ route('session.store') }}">
    {{ csrf_field() }}
    <p>
      <label for="movie_id">Movie</label>
      <select name="movie_id" id="movie_id" required
        class="block appearance-none w-full py-1 px-2 mb-1 text-base leading-normal bg-white text-grey-darker border border-grey rounded">
        @foreach ($movies as $movie)
          <option value="{{ $movie->id }}" {{ $movie->id === $movie_selected->id ? 'selected="selected"' : '' }}>
            {{ $movie->title }}
          </option>
        @endforeach
      </select>
      <x-error name="movie_id"></x-error>
    </p>
    <p>
      <label for="room_id">Cinema Room</label>
      <select name="room_id" id="room_id" required class="block appearance-none w-full py-1 px-2 mb-1 text-base leading-normal bg-white text-grey-darker border border-grey rounded">
        @foreach ($rooms as $room)
          <option value="{{ $room->id }}" {{ $room->id === $room_selected->id ? 'selected="selected"' : '' }}>
            {{ $room->cinema->name }}
          </option>
        @endforeach
      </select>
      <x-error name="room_id"></x-error>
    </p>
    <p>
      <label for="begin">Begin</label>
      <input type="text" name="begin" id="begin" value="" placeholder="2001-03-10 17:16:18" required
        class="block appearance-none w-full py-1 px-2 mb-1 text-base leading-normal bg-white text-grey-darker border border-grey rounded" />
      <x-error name="begin"></x-error>
    </p>

    <button type="submit"
      class="inline-block align-middle text-center select-none border font-normal whitespace-no-wrap py-2 px-4 rounded text-base leading-normal no-underline text-blue-lightest bg-blue hover:bg-blue-light py-3 px-4 text-xl leading-tight">Create</button>
  </form>
</x-app>
