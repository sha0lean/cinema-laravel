<x-app>
  <x-slot name="page_title">Edit {{ $session->room->cinema->name . ' ' . $session->room_id }}</x-slot>

  <form method="POST" action="{{ route('session.update', $session->id) }}">
    {{ csrf_field() }}
    {{ method_field('PUT') }}

    <p>
      <label for="movie_id">Movie</label>
      <select name="movie_id" id="movie_id" required
        class="block appearance-none w-full py-1 px-2 mb-1 text-base leading-normal bg-white text-grey-darker border border-grey rounded">
        @foreach ($movies as $movie)
          <option value="{{ $movie->id }}" {{ $movie->id === $session->movie_id ? 'selected="selected"' : '' }}>
            {{ $movie->title }}
          </option>
        @endforeach
      </select>
      <x-error name="movie_id"></x-error>
    </p>
    <p>
      <label for="room_id">Cinema Room</label>
      <select name="room_id" id="room_id" required class="block appearance-none w-full py-1 px-2 mb-1 text-base leading-normal bg-white text-grey-darker border border-grey rounded">
        @foreach ($rooms as $room)
          <option value="{{ $room->id }}" {{ $room->id === $session->room_id ? 'selected="selected"' : '' }}>
            {{ $room->cinema->name }}
          </option>
        @endforeach
      </select>
      <x-error name="room_id"></x-error>
    </p>
    <p>
      <label for="begin">Begin</label>
      <input type="text" name="begin" id="begin" value="{{ $session->begin }}" required
        class="block appearance-none w-full py-1 px-2 mb-1 text-base leading-normal bg-white text-grey-darker border border-grey rounded" />
      <x-error name="begin"></x-error>
    </p>

    <button type="submit"
      class="focus:outline-none text-white text-sm py-2.5 px-5 rounded-md bg-blue-500 hover:bg-blue-600 hover:shadow-lg">
      Update
    </button>
  </form>
</x-app>
